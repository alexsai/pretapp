/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

Ext.application({
    name: 'PretApp',

    requires: [
        'Ext.MessageBox',
        'Ext.Menu',
        'Ext.tab.Panel',
        'Ext.Map',
        'Ext.Label',
        'PretApp.util.Config'
    ],

    views: [
        'Main',
        'Login',
        'PretApp.view.util.CapturePicture',
        'store.Store',
        'store.StoresList',
        'store.StoreMap',
        'store.StoreInfo',
        'store.StoreProducts',
        'store.StoreProduct',
        'sales.SalesList',
        'sales.Sale',
        'sales.SaleUnlocked',
        'sales.SaleUsed',
        'user.AccountLogin',
        'user.FacebookLogin',
        'user.UserSaleList',
        'vendor.VendorPanel',
        'vendor.ManageProducts',
        'vendor.ManageSales',
        'vendor.NewSale',
        'vendor.NewProduct'
    ],
    
    controllers:[
        'StoreController',
        'SaleController',
        'UserController',
        'VendorController',
        'FacebookController'
    ],
    
    models:[
        'Product',
        'Store',
        'Sale'
    ],

    stores:[
        'Products',
        'Stores',
        'Sales',
        'VendorSales',
        'VendorProducts',
        'UserSales'
    ],
    
    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },
    phoneStartupScreen: '320x460',
    tabletStartupScreen: '320x460',
    desktopStartupScreen: '320x460',
    launch: function() {
        Ext.fly('appLoadingIndicator').destroy();
        Ext.Viewport.add(Ext.create('PretApp.view.Main'));

        if( PretApp.util.Config.isPhoneGap() ){
            // Facebook app setup
            FacebookInAppBrowser.init(
                PretApp.util.Config.getFacebookId(),
                PretApp.util.Config.getFacebookRedirect(),
                PretApp.util.Config.getFacebookPermissions()
            );
        }
        //alert(window.localStorage.getItem('accessToken'));
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
    
});
