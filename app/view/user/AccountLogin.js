Ext.define('PretApp.view.user.AccountLogin', {
    extend: 'Ext.form.Panel',
	xtype:'accountlogin',
	requires:[
		'Ext.form.FieldSet',
		'Ext.field.Password'
	],
	config: {
		items: [
            {
                xtype: 'fieldset',
                items: [
                    {
                        xtype: 'textfield',
                        id   : 'username',
                        name : 'username',
                        placeHolder: 'username',
                        required: true

                    },
                    {
                        xtype: 'passwordfield',
                        id   : 'password',
                        name : 'password',
                        placeHolder: 'password',
                        required: true

                    }
                ]
            },
            {
                xtype: 'button',
                text: 'Login',
                margin: '1em',
                id: 'accountloginbtn'
            }
        ]
	}
});
