Ext.define('PretApp.view.user.UserSaleList', {
    extend: 'Ext.List',
	xtype:'usersalelist',
	requires:[
		'PretApp.store.Sales'
	],
	config: {
		title:'',
		cls:'sales-list',
		itemTpl: '<div class="listItem"><div class="name">{storeName}</div> <div class="info">{amount}{type}  scade:{exiprationDate} stato:{userState}</div></div>',
		store: 'UserSales',
		grouped: false
	},
	initialize : function(){
		//if(PretApp.util.Config.getLogged())

		Ext.getStore('UserSales').getProxy().setExtraParams({
			userId: window.localStorage.getItem('fbname')
		});
		Ext.getStore('UserSales').load();
	}
});
