Ext.define('PretApp.view.user.FacebookLogin', {
    extend: 'Ext.form.Panel',
	xtype:'facebooklogin',
	requires:[
		
	],
	config: {
		html:'',
		margin: '1em'
	},
	initialize: function(){
		var redirectUrl = Ext.Object.toQueryString({
            redirect_uri: window.location.protocol + "//" + window.location.host + window.location.pathname,
            client_id: PretApp.app.facebookAppId,
            response_type: 'token'
        });
		console.log(redirectUrl);
		this.setHtml([
			'<a class="fbLogin" href="https://m.facebook.com/dialog/oauth?' + redirectUrl + '"></a>',
			'<div class="fb-facepile" data-app-id="' + PretApp.app.facebookAppId + '" data-max-rows="2" data-width="300"></div>'
			].join(''));
	}
});
