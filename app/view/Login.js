Ext.define('PretApp.view.Login', {
    extend: 'Ext.Container',
    xtype:'login',
    config: {
        layout: {
            layout: 'vbox',
            align: 'center'
        },
        defaults: {
            margin: 10
        },
        items: [
            {
                xtype:'button',
                layout:'fit',
                id:'fbloginbtn',
                html:'<div class="facebook-login"></div>'
            },
            {
                xtype:'button',
                html:'Area commercianti',
                cls: 'account-login',
                handler:function(){
                    this.up('navigationview').push({
                        title:'',
                        xtype:'accountlogin'
                    });
                }
            }
        ]
    }
});
