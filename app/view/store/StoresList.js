Ext.define('PretApp.view.store.StoresList', {
    extend: 'Ext.List',
	xtype:'storeslist',
	requires:[
		'PretApp.store.Stores'
	],
	config: {
		title:'',
		cls:'store-list',
		itemTpl: '<div class="listItem"><div class="name">{name}</div>'+
				'<div class="address">{address}</div><div class="info"> <strong>{products}</strong> capi</div>'+
			'</div>',
		store: 'Stores',
		grouped: true,
		indexBar: true
	}
});
// <strong>{followers}</strong> followers |