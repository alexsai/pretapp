Ext.define('PretApp.view.store.Store', {
    extend: 'Ext.Panel',
	xtype:'store',
	require:[
		'Ext.tab.Panel',
		'PretApp.view.store.StoreMap',
		'PretApp.view.store.StoreInfo'
	],	
	config:{
		cls: 'storePanel',
		
		items:[{
				html:'',
				centered: true,
				itemId: 'store-name',
				cls:'store-name'
			},
			{
				xtype:'tabpanel',
				cls:'store-tabs',
				tabBarPosition: 'top',
				docked:'bottom',
				styleHtmlContent: true,
				height:'90%',
				tabBar : {
					layout : { 
						pack : 'center' 
					} 
				},
				items: [{
					xtype:'storeproducts',
					itemId:'store-products'
				},
				{
					xtype:'storeinfo',
					itemId:'store-info'
				},
				{
					xtype:'storemap',
					itemId:'store-map'
				}]
		}]
	
	}
	
});
