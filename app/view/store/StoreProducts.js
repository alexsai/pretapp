Ext.define('PretApp.view.store.StoreProducts', {
    extend: 'Ext.dataview.DataView',
	requires:[
		'PretApp.store.Products',
        'PretApp.util.Config'
	],
	xtype:'storeproducts',
	fullscreen: true,
    storeId:1,
	config: {
		title:'PRODOTTI',
		store: 'Products',
		baseCls: 'products-list',
		itemTpl:[
			'<img class="image" src="'+PretApp.util.Config.getBaseResourcesUrl()+'/images/{storeId}/{id}.png" />'
            /*,'<div class="info">{name} {price}{currency}</div>'*/].join('')
	},
	loadProducts:function(storeId){
        this.storeId = storeId;
        Ext.Viewport.setMasked({
            xtype: 'loadmask'
        });
        productsView = this;
        this.getStore().load({
            //define the parameters of the store:
            params:{
                id : storeId
            },
            scope: this,
            callback : function(records, operation, success) {
                Ext.Viewport.setMasked(false); // hide the load screen
                //console.log(records);
                //console.log(operation);
           }
        });
        this.refresh();
	}
 
});

