Ext.define('PretApp.view.store.StoreMap', {
    extend: 'Ext.Map',
	xtype:'storemap',
	position:{lat:null,lon:null},
    config: {
        title: 'LOCATION',
        scrollable: null,
        listeners: {
            maprender: function(comp, map) {
				var position = new google.maps.LatLng(this.position.lat,this.position.lon);
                var me = this;
				me.setMapOptions({
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					zoomControl: false,
					streetViewControl:false,
					panControl: false,
					zoom: 16
				});
				
                var marker = new google.maps.Marker({
						map: map,
						position: position,
						title: "Tiramisu alle fragole"
					});
				setTimeout(function(){
                    me.getMap().setCenter(position);
                },2000);
            }
        }
    },
    setMapPosition: function(lat,lon){
    	console.log('myrender');
    	this.position.lat=lat;
    	this.position.lon=lon;
    }

});
