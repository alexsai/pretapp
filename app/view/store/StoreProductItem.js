Ext.define('PretApp.view.store.StoreProductItem', {
    extend: 'Ext.dataview.component.DataItem',
	xtype:'storeproductitem',
	fullscreen: true,
	config: {
        image: true,
 
        name: {
            cls: 'x-name',
            flex: 1
        },
 
        slider: {
            flex: 2
        },
 
        layout: {
            type: 'hbox',
            align: 'center'
        }
    }
});

