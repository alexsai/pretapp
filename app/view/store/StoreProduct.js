Ext.define('PretApp.view.store.StoreProduct', {
    extend: 'Ext.Sheet',
    xtype: 'product',
    require:['PretApp.util.Config'],
    config: {
        baseCls: 'product-view',
        centered: true,
        width: '90%',
        height: '90%',
        modal: true,
        hideOnMaskTap: true,

        layout: {
            type: 'fit'
    },

        items: [
            {
                id: 'product',
                cls: 'product',
                scrollable: true,
                tpl: new Ext.XTemplate(
					'<div class="product-container"><div class="image" style="background-image:url('+PretApp.util.Config.getBaseResourcesUrl()+'/images/{storeId}/{id}.png); width:365px; height:365px;"></div>'+
                        '<div class="info">'+
                            '<div class="name">{name}</div><div class="text">{price}{currency}</div>'+
                        '</div>'+
                    '</div>')
            }
        ],
		dockedItems: [{
				dock : 'right',
				xtype: 'button',
				text : 'Close',
				iconCls : 'delete',
				iconMask : true,
				handler: function () {
					sheet.hide();            
				}
			}
		],

        showAnimation: {
            type: 'fadeIn',
            duration: 250,
            easing: 'ease-out'
        },

        hideAnimation: {
            type: 'fadeOut',
            duration: 250,
            easing: 'ease-in'
        }
    },
    updateData: function(newData) {
        Ext.getCmp('product').setData(newData);
    }
});