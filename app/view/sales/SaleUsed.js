Ext.define('PretApp.view.sales.SaleUsed', {
    extend: 'Ext.Container',
	xtype:'saleused',
	require:[],
	config:{
		cls: 'salePanel',
		itemId:'usedSaleView',
		layout: {
	        type: 'vbox',
	        align: 'middle'
    	},
		items: [{
			xtype:'panel',
			cls:'sale-info-text',
			itemId:'unlocke-sale-info',
			html:'hai gia\' usufruito di questo sconto'
		}]
	}
	
});
