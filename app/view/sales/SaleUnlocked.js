Ext.define('PretApp.view.sales.SaleUnlocked', {
    extend: 'Ext.Container',
	xtype:'saleunlocked',
	require:[],
	config:{
		cls: 'salePanel',
		itemId:'unclockedSaleView',
		layout: {
	        type: 'vbox',
	        align: 'middle'
    	},
		items: [{
			xtype:'panel',
			cls:'sale-info',
			itemId:'unlocked-sale-info'
		},
		{
			xtype:'button',
			cls:'unlock-button',
			html:'USA SCONTO'
		},{
			xtype:'panel',
			cls:'sale-usage-warning',
			html:'(Da usare con il commerciante)'
		}]
	}
	
});
