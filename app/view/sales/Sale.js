Ext.define('PretApp.view.sales.Sale', {
    extend: 'Ext.Container',
	xtype:'sale',
	require:[],	
	config:{
		cls: 'salePanel',
		itemId:'saleView',
		scrollable : true,
		layout: {
	        type: 'vbox',
	        align: 'middle'
    	},
		items: [{
			xtype:'panel',
			cls:'sale-store-name',
			itemId:'sale-store-name'
		},{
			xtype:'panel',
			cls:'sale-conditions',
			itemId:'sale-conditions'
		},{
			xtype:'panel',
			cls:'sale-amount',
			itemId:'sale-amount'
		},{
			xtype:'button',
			cls:'unlock-button',
			html:'SBLOCCA'
		}]
	}
	
});
