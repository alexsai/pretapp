Ext.define('PretApp.view.sales.SalesList', {
    extend: 'Ext.List',
	xtype:'saleslist',
	requires:[
		'PretApp.store.Sales'
	],
	config: {
		title:'',
		cls:'sales-list',
		itemTpl: [	'<div class="listItem">',
						'<div class="name">{storeName}</div>',
						'<div class="info"><span class="amount">{amount}{type}</span>  sblocca entro il {endDate}</div>',
						'<div class="info">Hai ancora <strong>{daysLeft}</strong> giorni <strong>{hoursLeft}</strong> ore <strong>{minutesLeft}</strong> minuti</div>',
						'<div class="info"><strong>{state}</strong></div>',
					'</div>'].join(''),
		store: 'Sales',
		grouped: true
	}
});
