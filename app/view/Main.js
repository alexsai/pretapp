    Ext.define('PretApp.view.Main', {
        extend: 'Ext.NavigationView',
        xtype: 'main',
    	require:[
    		'PretApp.view.store.Store',
    		'PretApp.view.store.StoresList',
            'PretApp,view.Login',
            'Ext.Label',
    		'Ext.Menu'
    	],
    	fullscreen: true,
        config: {
        	itemId:'mainView',
        	defaultBackButtonText: '',
    		navigationBar: {
    			cls:'navBarTitle',
    			backButton: {
    				cls:'back-button',
    		       	iconCls: 'back',
    	            iconMask: true,
    	            text:''
    			},
    			items: [{
    					cls:'settings-button',
    					iconCls: 'list',
    					align: 'right',
    					handler: function() {
    						Ext.Viewport.toggleMenu('right');
    					}
    				}
    			]
    		},
    		items: [{
    				title:'',
    				items: [{
    							xtype: 'button',
    							text: '<h2>My brands</h2><h3>MODA, STILE E TENDENZE E STREET STYLE</h3>',
    							cls:'shopsBtn',
    							handler: function() {
    								this.up('navigationview').push({
    									title:'',
    									xtype:'storeslist'
    								});
    							}
    						},
    						{
    							xtype: 'button',
    							text: '<h2>Sblocca Sconti</h2><h3>Ottieni uno sconto in pochi secondi</h3>',
    							cls:'unlockSalesBtn',
    							handler: function() {
    								this.up('navigationview').push({
    									title: '',
    									xtype:'saleslist'
    								});
    							}
    						},
    						{
    							xtype: 'button',
    							text: '<h2>Hai un negozio?</h2><h3>Entra nel mondo Pretapporter</h3>',
    							cls:'offersBtn',
    							handler: function() {
    								this.up('navigationview').push({
    									title: '',
    									html: 'Offerte'
    								});
    							}
    						}
    				]
    		}]
        },
    	doSetHidden: function(hidden) {
            this.callParent(arguments);

            if (hidden) {
                Ext.Viewport.removeMenu('right');
            } else {
                Ext.Viewport.setMenu(this.menuForSide('right'), {
                    side: 'right',
                    reveal: true
                });
            }
        },
        menuForSide: function(side) {
        	var that = this;
            var items = [
            {
                text: 'Home',
                iconCls: 'home',
                scope: this,
                handler: function() {
                    Ext.ComponentQuery.query('#mainView')[0].pop(999);
                    Ext.Viewport.toggleMenu('right');
                }
            },
            {
                text: 'I miei sconti',
                iconCls: 'user',
                scope: this,
                handler: function() {
                    if(PretApp.util.Config.isUserLogged()){
                        Ext.ComponentQuery.query('#mainView')[0].pop(999);
                        Ext.ComponentQuery.query('#mainView')[0].push({
                            xtype:'usersalelist'
                        });
                        Ext.Viewport.toggleMenu('right');
                    }else{
                        Ext.Msg.show({
                            message: 'Non sei loggato. Accedere con Facebook?',
                            buttons:[{
                                id: 'yes',
                                iconCls: 'star',
                                iconMask: true,
                                text: 'Facebook Login',
                                ui:'confirm'
                            },{
                                id: 'no',
                                iconCls: 'delete',
                                iconMask: true,
                                text: 'Non ora',
                                ui:'decline'
                            }],
                            fn: function (btn) {
                                if(btn === 'Facebook Login'){
                                    PretApp.app.getController('FacebookController').onLogin();
                                }
                            }
                        });
                    }
                }
            },
            {
                text: 'Login',
                iconCls: 'user',
                id:'userloginbtn',
                scope: this,
                handler: function() {
                    Ext.ComponentQuery.query('#mainView')[0].pop(999);
                    Ext.ComponentQuery.query('#mainView')[0].push({
                        xtype:'login'
                    });
                    Ext.Viewport.toggleMenu('right');
                }
            },
            {
                xtype:'label',
                id:'sideMenuManageLabel',
                html:'Gestione Negozio',
                cls:'sideMenuLabel',
                hidden:true
            },
            {
                text: 'Gestisci Prodotti',
                id:'sideMenuManageProductsBtn',
                scope: this,
                hidden:true,
                handler: function() {
                    Ext.ComponentQuery.query('#mainView')[0].pop(999);
                    Ext.ComponentQuery.query('#mainView')[0].push({
                        xtype:'manageproducts'
                    });
                    Ext.Viewport.toggleMenu('right');
                }
            },
            {
                text: 'Gestisci Sconti',
                id:'sideMenuManageSalesBtn',
                scope: this,
                hidden:true,
                handler: function() {
                    Ext.ComponentQuery.query('#mainView')[0].pop(999);
                    Ext.ComponentQuery.query('#mainView')[0].push({
                        xtype:'managesales'
                    });
                    Ext.Viewport.toggleMenu('right');
                } 
            }];

            return Ext.create('Ext.Menu', {
                items: items
            });
        },
        onBackButtonTap:function(){
            var activeView = this.getActiveItem()._itemId;
            if(activeView==='unclockedSaleView' || activeView==='usedSaleView'){
                this.pop('saleslist');
            }
            else{
                this.callParent();
            }
        }
    });

