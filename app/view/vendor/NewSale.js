Ext.define('PretApp.view.vendor.NewSale', {
    extend: 'Ext.form.Panel',
	xtype:'newsale',
	requires:[
    'Ext.field.Number',
    'Ext.field.DatePicker'
	],
	config: {
		title:'',
		items: [{
                xtype: 'fieldset',
                items: [
                    {
                        xtype: 'numberfield',
                        label: 'Sconto %',
                        value: 5,
                        minValue: 5,
                        maxValue: 100,
                        stepValue: 5,
                        name:'amount'
                    },
                    {
                        xtype: 'datepickerfield',
                        label: 'Inizio',
                        name: 'startDate',
                        value: new Date()
                    },
                    {
                        xtype: 'datepickerfield',
                        label: 'Fine',
                        name: 'endDate',
                        value: Ext.Date.add(new Date(), Ext.Date.DAY, PretApp.util.Config.getMinDaysSaleUnlock())
                    },
                    {
                        xtype: 'datepickerfield',
                        label: 'Termine utilizzo',
                        name: 'expirationDate',
                        value: Ext.Date.add(new Date(), Ext.Date.DAY, PretApp.util.Config.getMinDaysSaleUnlock()+PretApp.util.Config.getMinDaysSaleUsage())
                    },
                    {
                        xtype: 'textareafield',
                        label: 'Condizioni',
                        maxRows: 5,
                        name: 'conditions'
                    }
                    ]
                },
                {
                    xtype: 'button',
                    text: 'Crea',
                    cls:'vendor-new-button',
                    margin: '1em',
                    id: 'createsale'
            }]
	}
});
