Ext.define('PretApp.view.vendor.ManageSales', {
    extend: 'Ext.Panel',
	xtype:'managesales',
	requires:[
		'PretApp.store.VendorSales',
		'PretApp.view.vendor.NewSale'
	],
	config: {
		layout:'fit',
		items:[{
			xtype:'button',
			docked: 'top',
			text:'Aggiungi Sconto',
			cls:'vendor-new-button',
			handler: function(){
				this.up('navigationview').push({
                        title:'',
                        xtype:'newsale'
                    });
			}
		},
		{
			xtype:'list',
			id:'vendorsaleslist',
			cls:'sales-list',
			itemTpl: '{amount} {type} Fine:{endDate}<br> Sbloccato: {unlockedCount} Stato:{state}'+
				'<div class="x-button related-btn" btnType="related" style="width:40px; height:40px; border: none; float:right; background: url(resources/images/trash.png) no-repeat;"></div>',
			store:'VendorSales'
		}]
	}
});
