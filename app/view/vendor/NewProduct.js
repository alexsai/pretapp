Ext.define('PretApp.view.vendor.NewProduct', {
    extend: 'Ext.form.Panel',
	xtype:'newproduct',
    id:'fileForm',
	requires:[
    'Ext.field.Number',
    'Ext.field.DatePicker',
    'Ext.field.File'
	],
	config: {
        name:'fileForm',
        layout:'vbox',
		title:'',
		items: [
                {
                    xtype: 'capturepicture',
                    id:'capturepicture',
                },
                {
                    xtype: 'textfield',
                    label: 'Nome',
                    name: 'productname'
                },
                {
                    xtype: 'numberfield',
                    label: 'Prezzo',
                    name: 'productprice'
                },
                {
                    xtype: 'textareafield',
                    label: 'Descrizione',
                    maxRows: 2,
                    name: 'productdescription'
                },
                {
                    xtype: 'button',
                    cls:'vendor-new-button',
                    text: 'Crea',
                    margin: '1em',
                    id: 'createproduct'
                }
            ]
	}
});
