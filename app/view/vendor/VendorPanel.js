Ext.define('PretApp.view.vendor.VendorPanel', {
    extend: 'Ext.Panel',
	xtype:'vendorpanel',
	requires:[
		'PretApp.view.vendor.ManageSales',
		'PretApp.view.vendor.ManageProducts'
	],
	config: {
		title:'',
		items: [{
				xtype: 'button',
				margin: '1em',
            	id   : 'loginUsername',
            	html : 'Gestisci Store',
            	handler:function(){
            		this.up('navigationview').push({
                        title:'',
                        xtype:'manageproducts'
                    });
            	}
            },
            {
            	xtype: 'button',
            	margin: '1em',
            	id   : 'loginPassword',
            	html : 'Gestisci Sconti',
            	handler:function(){
            		this.up('navigationview').push({
                        title:'',
                        xtype:'managesales'
                    });
            	}
        }]
	}
});
