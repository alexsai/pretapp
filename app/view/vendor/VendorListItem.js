Ext.define('PretApp.view.vendor.VendorListItem', {
    extend: 'Ext.dataview.component.DataItem',
    xtype: 'vendorlistitem',
    config: {
        cls: 'vendor-list-item',
        dataMap: {
            getHtml: {
                setHtml: 'name'
            },

            getButton: {
                setValue: 'cuteness'
            }
        },
        html: {
            cls: 'x-name',
            flex: 1
        },

        button: {
            flex: 2
        },
        layout: {
            type: 'hbox',
            align: 'center'
        }
    },
    applyHtml: function(config) {
        return Ext.factory(config, Ext.Component, this.getHtml());
    },
    updateHtml: function(newName, oldName) {
        if (newName) {
            this.add(newName);
        }

        if (oldName) {
            this.remove(oldName);
        }
    },
    applyButton: function(config) {
        return Ext.factory(config, Ext.Button, this.getButton());
    },
    updateButton: function(newButton, oldButton) {
        if (newButton) {
            this.add(newButton);
        }

        if (oldButton) {
            this.remove(oldButton);
        }
    }
});