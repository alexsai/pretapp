Ext.define('PretApp.view.vendor.ManageProducts', {
    extend: 'Ext.Panel',
	xtype:'manageproducts',
	requires:[
		'PretApp.store.VendorProducts',
		'PretApp.view.vendor.NewProduct',
		'PretApp.view.vendor.VendorListItem'
	],
	config: {
		layout:'fit',
		items:[{
			xtype:'button',
			docked: 'top',
			text:'Aggiungi Prodotto',
			cls:'vendor-new-button',
			handler: function(){
				this.up('navigationview').push({
                    title:'',
                    xtype:'newproduct'
                });
			}
        },
		{
			xtype:'list',
			id:'vendorproductlist',
			useComponents: true,
			cls:'products-list',
			itemTpl: '<div style="width:100%;text-align:left;vertical-align:middle;height:50px;"><img src="'+
				PretApp.util.Config.getBaseResourcesUrl()+
				'/images/1/{id}.png" height="40px" width="40px"/> {name} {price} euro'+
				'<div class="x-button related-btn" btnType="related" style="width:50px; height:50px; border: none; float:right; background: url(resources/images/trash.png) no-repeat;"></div></div>',
			store:'VendorProducts'
		}]
	}
});
