Ext.define('PretApp.store.Stores', {
    extend: 'Ext.data.Store',
    requires: ['PretApp.model.Store'],

    config :{
		storeId:'Stores',
        model: 'PretApp.model.Store',
        sorters:'name',
        grouper: {
            sortProperty: 'city',
            groupFn: function(record) {
                return record.get('city');
            }
        },
        proxy: {
            type: 'ajax',
            url: PretApp.util.Config.getBaseUrl()+'/stores',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        },
		autoLoad: true
    }
});
