Ext.define('PretApp.store.Sales', {
    extend: 'Ext.data.Store',
    requires: ['PretApp.model.Sale'],

    config :{
		storeId:'Sales',
        model: 'PretApp.model.Sale',
        sorters:'expiration',
        grouper: {
            sortProperty: 'city',
            groupFn: function(record) {
                return record.get('city');
            }
        },
        proxy: {
            type: 'ajax',
            url: PretApp.util.Config.getBaseUrl()+'/sales',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        },
        listeners: {
            refresh: function(store, data, eOpts) {
                store.each(function (item, index, length) {
                    item.setDaysLeft();
                    item.saleStateTranslate();
                });
            }
        },
        autoLoad: true
        // data: [{
        //     id: "1",
        //     storeId : "1",
        //     storeName : "Tiramisu alle Fragole",
        //     city : 'Torino',
        //     amount : '10',
        //     type : '%',
        //     startDate : '2014/04/01',
        //     endDate : '2014/05/15',
        //     exiprationDate: '2014/04/30',
        //     state :'usato',
        //     conditions: 'Condizioni sconto'
        // }, 
        // {
        //     id: "2",
        //     storeId : '2',
        //     storeName : "Fashion Shop",
        //     city : 'Torino',
        //     amount : '15',
        //     type:'%',
        //     startDate : '2014/04/01',
        //     endDate : '2014/05/30',
        //     exiprationDate: '2014/04/30',
        //     state :'',
        //     conditions: 'Condizioni sconto'
        // },
        // {
        //     id: "3",
        //     storeId : "3",
        //     storeName : "Tiramisu alle Fragole",
        //     city : 'Milano',
        //     amount : '10',
        //     type : '%',
        //     startDate : '2014/04/01',
        //     endDate : '2014/06/30',
        //     exiprationDate: '2014/04/30',
        //     state :'sbloccato',
        //     conditions: 'Condizioni sconto'
        // }]
    }
});
