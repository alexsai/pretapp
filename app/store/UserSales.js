Ext.define('PretApp.store.UserSales', {
    extend: 'Ext.data.Store',
    requires: ['PretApp.model.Sale'],
    config :{
		storeId:'UserSales',
        model: 'PretApp.model.Sale',
        sorters:'expirationDate',
        grouper: {
            sortProperty: 'city',
            groupFn: function(record) {
                return record.get('city');
            }
        },
        proxy: {
            type: 'ajax',
            url: PretApp.util.Config.getBaseUrl()+'/usersales',
            reader: {
                type: 'json',
                rootProperty: ''
            },
            extraParams:{
                userId:0
            }
        },
        listeners: {
            refresh: function(store, data, eOpts) {
                store.each(function (item, index, length) {
                    item.setDaysLeft();
                });
            }
        },
		autoLoad: false,
        // data :[{
        //     id: "1",
        //     storeId : "1",
        //     storeName : "Tiramisu alle Fragole",
        //     city : 'Torino',
        //     amount : '10',
        //     type : '%',
        //     startDate : '2014/04/01',
        //     endDate : '2014/04/01',
        //     exiprationDate: '2014/04/30',
        //     state :'usato',
        //     conditions: 'Condizioni sconto'
        // }, 
        // {
        //     id: "3",
        //     storeId : "3",
        //     storeName : "Tiramisu alle Fragole",
        //     city : 'Milano',
        //     amount : '10',
        //     type : '%',
        //     startDate : '2014/04/01',
        //     endDate : '2014/04/01',
        //     exiprationDate: '2014/04/30',
        //     state :'sbloccato',
        //     conditions: 'Condizioni sconto'
        // }]
    }
});
