Ext.define('PretApp.store.VendorProducts', {
    extend: 'Ext.data.Store',
    requires: ['PretApp.model.Product'],
    config :{
		storeId:'VendorProducts',
        model: 'PretApp.model.Product',
        proxy: {
            type: 'ajax',
            url: PretApp.util.Config.getBaseUrl()+'/vendor/products',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        }
    }
});
