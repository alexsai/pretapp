Ext.define('PretApp.store.Products', {
    extend: 'Ext.data.Store',
    requires: ['PretApp.model.Product'],

     config :{
        storeId:'Products',
        model: 'PretApp.model.Product',
        proxy: {
            type: 'ajax',
            url: PretApp.util.Config.getBaseUrl()+'/store-products',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        },
        autoLoad: false
    }
    /*
        data: [{
                id: "1",
                name: "Vestito",
				price: "45.00",
				imageUrl : "1"
            },
			{
                id: "2",
                name: "Vestito",
				price: "45.00",
				imageUrl : "2"
            },
			{
                id: "3",
                name: "Vestito",
				price: "45.00",
				imageUrl : "3"
            }
			,{
                id: "4",
                name: "Vestito",
				price: "45.00",
				imageUrl : "4"
            },
			{
                id: "5",
                name: "Vestito",
				price: "45.00",
				imageUrl : "5"
            },
			{
                id: "6",
                name: "Vestito",
				price: "45.00",
				imageUrl : "6"
            }
			]
    */
});
