Ext.define('PretApp.store.VendorSales', {
    extend: 'Ext.data.Store',
    requires: ['PretApp.model.Sale'],
    config :{
		storeId:'VendorSales',
        model: 'PretApp.model.Sale',
        sorters:'expiration',
        proxy: {
            type: 'ajax',
            url: PretApp.util.Config.getBaseUrl()+'/vendor/sales',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        },
        listeners: {
            refresh: function(store, data, eOpts) {
                store.each(function (item, index, length) {
                    item.setDaysLeft();
                    item.saleStateTranslate();
                    item.saleVendorStateTranslate();
                });
            }
        },
    }
});
