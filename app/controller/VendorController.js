Ext.define('PretApp.controller.VendorController', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.MessageBox'],    
    config: {
        refs: {
            main: 'main',
            accountlogin:'accountlogin',
            newsale:'newsale',
            newproduct:'newproduct',
            managesaleslist:'managesales list',
            manageproductslist:'manageproducts list'
        },
        control: {
            '#accountloginbtn':{
                tap: 'onVendorLogin'
            },
            '#createproduct':{
                tap: 'onProductCreate'
            },
            '#createsale':{
                tap: 'onCreateSale'
            },
            'manageproductslist':{
                itemtap:'onProductDelete'
            },
            'managesaleslist':{
                itemtap:'onSaleDelete'
            }
        }
    },
    storeId : null,
    onVendorLogin: function(){
        var that = this;
        var values = this.getAccountlogin().getValues();
        var jsonToPost = {};
        jsonToPost.username = values.username;
        jsonToPost.password = values.password;
        Ext.Ajax.request({
            url: PretApp.util.Config.getBaseUrl()+'/login',
            method: 'POST',
            params: jsonToPost,
            success: function(response) {
                var json = Ext.JSON.decode(response.responseText);
                if(json.status === 0){                
                    //window.localStorage.setItem('vendorStoreId',response.responseText.storeId);
                    that.storeId = json.message;
                    PretApp.util.Config.setVendorStoreId(json.message);
                    that.getMain().push({
                        xtype:'vendorpanel'
                    });
                    that.showVendorMenu();
                    that.onLoadVendorData();
                }else{
                    Ext.Msg.show({
                        message: json.message,
                        buttons:[{
                            id: 'ok',
                            iconCls: 'star',
                            iconMask: true,
                            text: 'ok',
                            ui:'confirm'
                        }],
                    });
                }
            },
            failure: function(response) {
                 Ext.Msg.show({
                    message: 'Impossibile eseguire il login.',
                    buttons:[{
                        id: 'ok',
                        iconCls: 'star',
                        iconMask: true,
                        text: 'ok',
                        ui:'confirm'
                    }],
                });
            }
        });
    },
    onLoadVendorData : function(){
        var that = this;

        var salesStore = Ext.getStore('VendorSales');
        salesStore.getProxy().setExtraParam('storeId', PretApp.util.Config.getVendorStoreId());
        salesStore.load();

        var productsStore = Ext.getStore('VendorProducts');
        productsStore.getProxy().setExtraParam('storeId', PretApp.util.Config.getVendorStoreId());
        productsStore.load();
    },
    onSaleDelete: function (dataview,index,list,record, tar, obj) {
        var tappedItem = tar.getTarget('div.x-button');
        var btntype = tappedItem.getAttribute('btnType');
        if(btntype == 'related'){
            Ext.Msg.show({
            message: 'Cancellare sconto da '+record.data['amount']+'% che termina il '+record.data['endDate'],
            buttons:[{
                id: 'no',
                iconCls: 'delete',
                iconMask: true,
                text: 'Annulla',
                ui:'decline'
            },{
                id: 'yes',
                iconCls: 'star',
                iconMask: true,
                text: 'Procedi',
                ui:'confirm'
            }],
            fn: function (btn) {
                if(btn == 'Procedi'){
                    Ext.Ajax.request({
                        url: PretApp.util.Config.getBaseUrl()+'/vendor/deletesale',
                        method: 'GET',
                        disableCaching:false,
                        headers: { 'Content-Type': 'application/json; charset=UTF-8' },
                        params : {
                            storeId : record.data['storeId'],
                            saleId : record.data['id']
                        },
                        success: function (response) {
                            Ext.getStore('VendorSales').load();
                        },
                        failure: function (response) {
                             Ext.Msg.show({
                                message: 'Si è verificato un errore durante la cancellazione.',
                                buttons:[{
                                    id: 'ok',
                                    iconCls: 'star',
                                    iconMask: true,
                                    text: 'ok',
                                    ui:'confirm'
                                }],
                            });
                        }
                    });
                }
            }
        });
        }
    },
    onProductDelete: function (dataview,index,list,record, tar, obj) {
        var tappedItem = tar.getTarget('div.x-button');
        var btntype = tappedItem.getAttribute('btnType');
        if(btntype == 'related')
        {
            Ext.Msg.show({
            message: 'Cancellare '+record.data['name']+'da '+record.data['prezzo']+' '+record.data['currency'],
            buttons:[{
                id: 'no',
                iconCls: 'delete',
                iconMask: true,
                text: 'Annulla',
                ui:'decline'
            },{
                id: 'yes',
                iconCls: 'star',
                iconMask: true,
                text: 'Procedi',
                ui:'confirm'
            }],
            fn: function (btn) {
                if(btn == 'Procedi'){
                    Ext.Ajax.request({
                        url: PretApp.util.Config.getBaseUrl()+'/vendor/deleteproduct',
                        method: 'GET',
                        disableCaching:false,
                        headers: { 'Content-Type': 'application/json; charset=UTF-8' },
                        params : {
                            storeId : record.data['storeId'],
                            productId : record.data['id']
                        },
                        success: function (response) {
                            Ext.getStore('VendorProducts').load();
                        },
                        failure: function (response) {
                            Ext.Msg.show({
                                message: 'Si è verificato un errore durante la cancellazione.',
                                buttons:[{
                                    id: 'ok',
                                    iconCls: 'star',
                                    iconMask: true,
                                    text: 'OK',
                                    ui:'confirm'
                                }]
                            });
                        }
                    });
                }
            }
        });
        }
    },
    onCreateSale: function() {
        var that = this;
        var values = this.getNewsale().getValues();
       
        if(this.storeId === null){
            that.getMain().pop(999);
            that.getMain().push({
                title:'',
                xtype:'accountlogin'
            });
            return;
        }

        var now = new Date();
        if( Ext.Date.diff(values.startDate, now, Ext.Date.DAY) > 0 ){
            Ext.Msg.alert('', 'La promozione deve iniziare almeno da oggi', Ext.emptyFn);
            return;
        }
        
        if( Ext.Date.diff(values.startDate, values.endDate,  Ext.Date.DAY) + 2 < PretApp.util.Config.getMinDaysSaleUnlock() ){
            Ext.Msg.alert('', 'La durata minima tra l\'inizio e la fine della promozione è di '+PretApp.util.Config.getMinDaysSaleUnlock()+' giorni.' , Ext.emptyFn);
            return;
        }
        
        if( Ext.Date.diff(values.endDate, values.expirationDate,  Ext.Date.DAY) + 2 < PretApp.util.Config.getMinDaysSaleUsage() ){
            Ext.Msg.alert('', 'La durata di utilizzo della promozione dalla fine della promozione è di '+PretApp.util.Config.getMinDaysSaleUsage()+' giorni.' , Ext.emptyFn);
            return;
        }

        if(values.amount === ''){
            Ext.Msg.alert('', 'Inserisci l\'ammontare dello sconto.', Ext.emptyFn);
            return;
        }

        var jsonToPost = {};

        jsonToPost.startDate = Ext.Date.format(values.startDate,'Y-m-d H:i:s');
        jsonToPost.endDate = Ext.Date.format(values.endDate,'Y-m-d H:i:s');
        jsonToPost.expirationDate = Ext.Date.format(values.expirationDate,'Y-m-d H:i:s');
        jsonToPost.amount = values.amount;
        jsonToPost.conditions = values.conditions;
        jsonToPost.saletype = '%';
        jsonToPost.storeId = PretApp.util.Config.getVendorStoreId();

        Ext.Ajax.request({
            url: PretApp.util.Config.getBaseUrl()+'/vendor/addsale',
            method: 'POST',
            params: jsonToPost,
            success: function(response) {
                Ext.ComponentQuery.query('#vendorsaleslist')[0].getStore().load();
                that.getMain().pop();
            },
            failure: function(response) {
                Ext.Msg.show({
                    message: 'Si è verificato un errore nella creazione della promozione',
                    buttons:[{
                        id: 'ok',
                        iconCls: 'star',
                        iconMask: true,
                        text: 'OK',
                        ui:'confirm'
                    }]
                });
                that.getMain().pop();
            }
        });
    },
    onProductCreate: function() {
        var that = this;
        var values = this.getNewproduct().getValues();
        var jsonToPost = {};

        jsonToPost.storeId = this.storeId;

        if(this.storeId === null){
            that.getMain().pop(999);
            that.getMain().push({
                title:'',
                xtype:'accountlogin'
            });
            return;
        }

        if(values.productname === '' || Ext.ComponentQuery.query('#capturepicture')[0].getImageDataUrl() === undefined ){
            Ext.Msg.alert('', 'Inserisci nome e foto del prodotto.', Ext.emptyFn);
            return;
        }

        jsonToPost.name = values.productname;
        jsonToPost.description = values.productdescription;
        jsonToPost.price = values.productprice;
        jsonToPost.uploadedFile = Ext.ComponentQuery.query('#capturepicture')[0].getImageDataUrl();

        Ext.Ajax.request({
            url: PretApp.util.Config.getBaseUrl()+'/vendor/addproduct',
            //headers: {'Content-type':'multipart/form-data'},
            method: 'POST',
            params: jsonToPost,
            success: function(response) {
                Ext.getStore('VendorProducts').load();
                that.getMain().pop();
            },
            failure: function(response) {
                Ext.Msg.show({
                    message: 'Si è verificato un errore nella creazione del prodotto.',
                    buttons:[{
                        id: 'ok',
                        iconCls: 'star',
                        iconMask: true,
                        text: 'OK',
                        ui:'confirm'
                    }]
                });
                that.getMain().pop();
            }
        });
    },
    onAccountLogout: function() {
        PretApp.util.Config.setVendorId(null);
        // var that = this;
        // var values = this.getAccountlogin().getValues();
        // var jsonToPost = {};
        // jsonToPost.username = values.username;
        // jsonToPost.password = values.password;
        // Ext.Ajax.request({
        //     url: PretApp.util.Config.getBaseUrl()+'/accountlogin',
        //     method: 'POST',
        //     params: jsonToPost,
        //     success: function(response) {
        //     },
        //     failure: function(response) {
        //     }
        // });
    },
    showVendorMenu: function() {
        Ext.ComponentQuery.query('#sideMenuManageLabel')[0].show();
        Ext.ComponentQuery.query('#sideMenuManageProductsBtn')[0].show();
        Ext.ComponentQuery.query('#sideMenuManageSalesBtn')[0].show();
    },
    hideVendorMenu: function() {
        Ext.ComponentQuery.query('#sideMenuManageLabel')[0].hide();
        Ext.ComponentQuery.query('#sideMenuManageProductsBtn')[0].hide();
        Ext.ComponentQuery.query('#sideMenuManageSalesBtn')[0].hide();
    }
});
