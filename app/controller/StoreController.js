Ext.define('PretApp.controller.StoreController', {
    extend: 'Ext.app.Controller',
    require:['PretApp.util.Config'],
    config: {
        refs: {
            main: 'main',
			storeProducts: 'storeproducts',
            storeslist :'storeslist',
			productView: {
                autoCreate: true,
                xtype: 'product',
                selector: 'product'
            }
        },
        //before: {
         //   showRootCategory: 'ensureStoreLoad',
         //   showCategoryById: 'ensureStoreLoad'
        //},
		init: function() {
			this.callParent();
			this.productView = Ext.create('PretApp.view.store.StoreProduct');
		},
        control: {
            storeProducts: {
				itemtap: 'onProductTap'
            },
            storeslist:{
                itemtap:'onStoreTap'
            }
        },
        routes: {
        },
        currentRecord: null,
        stack: []
    },
    onStoreTap: function(list, index, el,record) {
        this.getMain().push({
            xtype:'store'
        });
        var store = Ext.ComponentQuery.query('#store-name')[0].getParent();
        store.down('#store-info').setHtml('<img src="'+PretApp.util.Config.getBaseResourcesUrl()+'/images/'+record.get('id')+'/logo.jpg" ><div class="info-field">'+record.get('info')+'</div>');
        store.down('#store-name').setHtml('<h2 class="store-title">'+record.get('name')+'</h2>');
        store.down('#store-map').setMapPosition(record.get('lat'),record.get('lon')) ;
        store.down('#store-products').loadProducts(record.get('id'));
    },
    onProductTap:  function(dataview, index, target, record, e, eOpts)  {
        var productView = this.getProductView();
        productView.setData(record.data);
        if (!productView.getParent()) {
            Ext.Viewport.add(productView);
        }
        productView.show();
    }
});
