Ext.define('PretApp.controller.UserController', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.MessageBox'],    
    config: {
        // refs: {
        //     main: 'main',
        //     accountlogin:'accountlogin',
        //     facebooklogin: 'facebooklogin',
        //     usersalelist :'usersaleslist'
        // },
        // control: {
        //     usersaleslist:{
        //         itemtap:'onUserSale'
        //     },
        //     '#accountloginbtn':{
        //         tap: 'onAccountLogin'
        //     },
        //     '#signout': {
        //         tap: 'onUserTap'
        //     },
        //     '#logoutButton': {
        //         tap: 'logout'
        //     }
        // }
    },
    onUserSale:function(list, index, el, record){
        console.log('usersalelist');
        // care
        // this.getMain().push({
        //     xtype:'unlockedsale'
        // });
        
        // var sale = Ext.ComponentQuery.query('#saleView')[0];
        // sale.setData(record.data);
        // sale.down('#sale-store-name').setHtml('<img src="resources/images/'+record.get('logo')+'" ><div class="info-field">'+record.get('info')+'</div>');
        // sale.down('#sale-store-name').setHtml('<h2>'+record.get('storeName')+'</h2>');
        // sale.down('#sale-conditions').setHtml('<p class="sale-info">'+record.get('city') +'<br>Sblocca entro<br>'+record.get('endDate')+'</p>');
        // sale.down('#sale-amount').setHtml('<div class="sale-canvas"><div class="inner-border"><div class="amount">'+record.get('amount')+record.get('type')+'</div></div></div>');
    },
    onAccountLogin: function() {
        var that = this;
        var values = this.getAccountlogin().getValues();
        var jsonToPost = {};
        jsonToPost.username = values.username;
        jsonToPost.password = values.password;
        Ext.Ajax.request({
            url: PretApp.util.Config.getBaseUrl()+'/accountlogin',
            method: 'POST',
            params: jsonToPost,
            success: function(response) {
                
                that.getMain().push({
                    xtype:'vendorpanel'
                });
                console.log(response.responseText);
                PretApp.util.Config.setVendorId(response.responseText.vendorId);
                that.showVendorMenu();
            },
            failure: function(response) {
                that.getMain().push({
                    xtype:'vendorpanel'
                });
                console.log(response.responseText);
                PretApp.util.Config.setVendorId(response.responseText.vendorId);
                that.showVendorMenu();
            }
        });
    },
    onAccountLogout: function() {
        PretApp.util.Config.setVendorId(null);
        // var that = this;
        // var values = this.getAccountlogin().getValues();
        // var jsonToPost = {};
        // jsonToPost.username = values.username;
        // jsonToPost.password = values.password;
        // Ext.Ajax.request({
        //     url: PretApp.util.Config.getBaseUrl()+'/accountlogin',
        //     method: 'POST',
        //     params: jsonToPost,
        //     success: function(response) {
        //     },
        //     failure: function(response) {
        //     }
        // });
    },
    showVendorMenu: function() {
        Ext.ComponentQuery.query('#sideMenuManageLabel')[0].show();
        Ext.ComponentQuery.query('#sideMenuManageProductsBtn')[0].show();
        Ext.ComponentQuery.query('#sideMenuManageSalesBtn')[0].show();
    },
    hideVendorMenu: function() {
        Ext.ComponentQuery.query('#sideMenuManageLabel')[0].hide();
        Ext.ComponentQuery.query('#sideMenuManageProductsBtn')[0].hide();
        Ext.ComponentQuery.query('#sideMenuManageSalesBtn')[0].hide();
    }
});
