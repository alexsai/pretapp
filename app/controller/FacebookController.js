Ext.define('PretApp.controller.FacebookController', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.MessageBox'],
    config: {
        refs: {
            loginview: 'login'
        },
        control: {
            '#fbloginbtn':{
                tap: 'onLogin'
            }
        }
    },
    onLogin : function() {
        console.log('facebooklogin');
        FacebookInAppBrowser.login({
            send: function() {},
            success: function(access_token) {
                //alert('done, access token: ' + access_token);
            },
            denied: function() {
                Ext.Msg.alert('L\'accesso a facebook è necessario per sbloccare gli sconti ed usufruire degli sconti già abloccati.',
                    'Non ti preoccupare Pretapporter non pubblicherà niente sul tuo profilo senza una tua diretta autorizzazione.',
                    Ext.emptyFn);
            },
            complete: function(access_token) {},
            userId: function(userId) {}
        });
    },
    onLogout: function(){
        FacebookInAppBrowser.logout();
    },
    post: function(){
      FacebookInAppBrowser.post({
        name: 'My post',
        link: 'http://frop.me',
        message: 'Try this out',
        picture: 'http://caiovaccaro.com.br/wp-content/uploads/2013/10/frop01.jpg',
        description: 'Sent trough mobile app'}, function(response) {
          if(response) {
            alert('post successful');
          }
        })
      }
});