Ext.define('PretApp.controller.SaleController', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            main: 'main',
            saleslist :'saleslist',
            saleUnlockBtn:'sale button',
            saleUseBtn:'saleunlocked button'
        },
        control: {
            saleslist:{
                itemtap:'onSaleTap'
            },
            saleUnlockBtn:{
                tap:'onUnlockSale'
            },
            saleUseBtn:{
                tap:'onUseSale'
            },
            main:{
                back:'onBack'
            }
        },
        currentRecord: null,
        stack: []
    },
    onBack : function(){
        
        //console.log('back');
        //console.log(this.getMain().getItems().getCount());
        
        var items = this.getMain().getItems();
        if(items.getCount() >= 5){
            this.getMain().pop('saleslist');
            Ext.ComponentQuery.query('saleslist')[0].refresh();
            return;
        }        
    },
    onSaleTap: function(list, index, el,record) {
        this.getMain().push({
            xtype:'sale'
        });
        
        var sale = Ext.ComponentQuery.query('#saleView')[0];
        sale.setData(record.data);
        //sale.down('#sale-store-name').setHtml('<img src="resources/images/'+record.get('logo')+'" ><div class="info-field">'+record.get('info')+'</div>');
        sale.down('#sale-store-name').setHtml('<h2>'+record.get('storeName')+'</h2>');
        sale.down('#sale-conditions').setHtml('<p class="sale-info">'+record.get('city') +'<br>Sblocca entro<br>'+record.get('endDate')+'</p>');
        sale.down('#sale-amount').setHtml('<div class="sale-canvas"><div class="inner-border"><div class="amount">'+record.get('amount')+record.get('type')+'</div></div></div>');
        
    },
    login : function() {
        FacebookInAppBrowser.login({
            send: function() {},
            success: function(access_token) {
                //alert('done, access token: ' + access_token);
            },
            denied: function() {
                Ext.Msg.alert('L\'accesso a facebook è necessario per sbloccare gli sconti ed usufruire degli sconti già abloccati.',
                    'Non ti preoccupare Pretapporter non pubblicherà niente sul tuo profilo senza una tua diretta autorizzazione.',
                    Ext.emptyFn);
            },
            complete: function(access_token) {},
            userId: function(userId) {}
        });
    },
    onUnlockSale: function() {
        var that = this;
        // this.login();
        // FacebookInAppBrowser.post({
        //     name: 'Pretapporter',
        //     link: 'http://www.pretapporter.it',
        //     message: window.localStorage.getItem('fbname') + ' ha sbloccato uno sconto del 15% da Tiramisù alle Fragole tramite Pretapporter.',
        //     picture: 'https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-prn2/t1.0-9/1176222_567734366616376_1513001817_n.jpg',
        //     description: 'Tanti sconti ti aspettano su Pretapporter per i tuoi negozi preferiti, scarica l\'applicazione per Iphone e Android'
        // },
        //     function(response) {
        //         if(response) {
        //             alert(that);
        //             var thisScope = that;
        //             // Facebook share success
        //             var saleData = Ext.ComponentQuery.query('#sale-store-name')[0].getParent().getData();
        //             

        //             Ext.Ajax.request({
        //                 url: PretApp.util.Config.getBaseUrl()+'/unlocksale',
        //                 method: 'GET',
        //                 disableCaching:false,
        //                 headers: { 'Content-Type': 'application/json; charset=UTF-8' },
        //                 params : {
        //                     userId : facebookUserId,
        //                     saleId : saleData.id
        //                 },
        //                 success: function (response) {
        //                     // Pretapporter unlock success
        //                     thisScope.getMain().push({
        //                         xtype:'saleunlocked'
        //                     });
        //                     var unlockedSale = Ext.ComponentQuery.query('#unlocked-sale-info')[0].getParent();
        //                     unlockedSale.down('#unlocked-sale-info-1').setHtml(
        //                         'Complimenti '+window.localStorage.getItem('fbname')+' ! Hai sbloccato'+'<br>'+
        //                         saleData.amount+saleData.type+'<br>'+
        //                         'da '+ saleData.storeName+'<br>'+
        //                         'da usare entro' +saleData.expiration+'<br>'

        //                     );

        //                 },
        //                 failure: function (response) {

        //                     alert('Impossibile sbloccare lo sconto.');
        //                     that.getMain().push({
        //                         xtype:'saleunlocked'
        //                     });
        //                     var unlockedSale = Ext.ComponentQuery.query('#unlocked-sale-info')[0].getParent();
        //                     console.log(unlockedSale.getData());
        //                     unlockedSale.down('#unlocked-sale-info-1').setHtml(
        //                         'Complimenti! Hai sbloccato'+'<br>'+
        //                         saleData.amount+saleData.type+'<br>'+
        //                         'da '+ saleData.storeName+'<br>'+
        //                         'da usare entro' +saleData.expiration+'<br>'

        //                     );
        //                 }
        //             }); 

        //         }else{
        //             alert('Impossibile condividere tramite Facebook. Riprova più tardi.');
        //         }
        // });
        var saleData = Ext.ComponentQuery.query('sale')[0].getData();

        Ext.Ajax.request({
            url: PretApp.util.Config.getBaseUrl()+'/unlocksale',
            method: 'GET',
            disableCaching:false,
            headers: { 'Content-Type': 'application/json; charset=UTF-8' },
            params : {
                userId : window.localStorage.getItem('uid'),
                saleId : saleData.id
            },
            success: function (response) {

            },
            failure: function (response) {

                //alert('Impossibile sbloccare lo sconto.');
                that.getMain().push({
                    xtype:'saleunlocked'
                });
                // QUESTO
                var unlockedSale = Ext.ComponentQuery.query('saleunlocked')[0];
                unlockedSale.setData(saleData);
                unlockedSale.down('#unlocked-sale-info').setHtml(
                    window.localStorage.getItem('fbname') + ' hai sbloccato'+'<br>'+
                    '<h3>'+saleData.amount+saleData.type+'</h3>'+
                    '<h3>'+ saleData.storeName+'</h3>'+
                    'valido fino al <h3>' +saleData.exiprationDate+'</h3>'+
                    '<h3>Visita negozio</h3>'

                );
            }
        });         
        
        

        /*
        var datasale = Ext.ComponentQuery.query('#saleView')[0].getData();
        var obj = new Object();
        obj.userId = 1;
        obj.saleId = datasale.id;
        var data = Ext.JSON.encode(obj);
        Ext.Ajax.request({
                        url: PretApp.util.Config.getBaseUrl()+'/unlocksale',
                        method: 'GET',
                        disableCaching:false,
                        headers: { 'Content-Type': 'application/json; charset=UTF-8' },
                        params : {
                            userId : 1,
                            saleId : 1
                        },
                        success: function (response) {
                            that.getMain().push({
                                xtype:'saleunlocked'
                            });
                            
                            var saleData = Ext.ComponentQuery.query('#sale-store-name')[0].getParent().getData();
                            var unlockedSale = Ext.ComponentQuery.query('#unlocked-sale-info-1')[0].getParent();
                            unlockedSale.setData(saleData);
                            unlockedSale.down('#unlocked-sale-info-1').setHtml('<h2>Complimenti! Hai sbloccato</h2>');
                            unlockedSale.down('#unlocked-sale-amount').setHtml(
                                '<div class="sale-canvas"><div class="inner-border"><div class="amount">'+saleData.amount+' '+saleData.type+'</div></div></div>');
                            unlockedSale.down('#unlocked-sale-info-2').setHtml(
                                'presso<br><p class="sale-info">'+ saleData.storeName+'</p>'+
                                'da usare entro<br><p class="sale-info">' +saleData.endDate+'</p>');
                            //unlockedSale.down('#unlocked-sale-amount').setHtml('');

                        },
                        failure: function (response) {
                            alert('unlocksale errore');
                        }
        });
        */
        // SENZA FACE END LEVA IL FALSE SOTTO
        

        /*
        if(false){ // BLOCCO POSTTTTTTTTTTTTTT
        FB.login(function(response) {
          if (response.authResponse) {
            var facebookUserId = response.authResponse.userId;
            FB.api('/me/feed', 'post', { message: "t" }, function(response) {
                if (!response || response.error) {
                    alert('facebook error: impossibile postare');
                } else {
                    alert('Facebook post ok');
                    Ext.Ajax.request({
                        url: PretApp.util.Config.getBaseUrl()+'/unlocksale',
                        method: 'GET',
                        disableCaching:false,
                        headers: { 'Content-Type': 'application/json; charset=UTF-8' },
                        params : {
                            userId : facebookUserId,
                            saleId : saleData.id
                        },
                        success: function (response) {
                            that.getMain().push({
                                xtype:'saleunlocked'
                            });
                            
                            var saleData = Ext.ComponentQuery.query('#sale-store-name')[0].getParent().getData();
                            var unlockedSale = Ext.ComponentQuery.query('#unlocke-sale-info')[0].getParent();
                            console.log(unlockedSale.getData());
                            unlockedSale.down('#unlocke-sale-info').setHtml(
                                'Complimenti! Hai sbloccato'+'<br>'+
                                saleData.amount+saleData.type+'<br>'+
                                'da '+ saleData.storeName+'<br>'+
                                'da usare entro' +saleData.expiration+'<br>'

                            );

                        },
                        failure: function (response) {
                            alert('unlocksale errore');
                        }
                    });       

                    
                }
            });


             //POST WITH DIALOG
                // FB.ui({
                //     method: 'feed', 
                //     name: 'Facebook Dialogs',
                //     link: 'https://developers.facebook.com/docs/reference/dialogs/',
                //     picture: 'http://fbrell.com/f8.jpg',
                //     caption: 'Reference Documentation',
                //     description: 'Dialogs provide a simple, consistent interface for applications to interface with users.'
                // },
                // function(response) {
                //   if (response && response.post_id) {
                //     alert('Post was published.');
                //   } else {
                //     alert('Post was not published.');
                //   }
                // });
            
          } else {
            alert('User cancelled login or did not fully authorize.');
          }
        }, {scope: 'user_likes,offline_access,publish_stream'});
        }
        */
    },
    onUseSale: function() {
        console.log('sale used');
        var that = this;
        var saleData = Ext.ComponentQuery.query('saleunlocked')[0].getData();
        //if(!logged) alert('loggalo prima');
        Ext.Msg.show({
            message: 'Attenzione stai per utilizzare lo sconto.<br>Questa operazione e\' irreversibile',
            buttons:[{
                id: 'no',
                iconCls: 'delete',
                iconMask: true,
                text: 'Annulla',
                ui:'decline'
            },{
                id: 'yes',
                iconCls: 'star',
                iconMask: true,
                text: 'Procedi',
                ui:'confirm'
            }],
            fn: function (btn) {
                if(btn == 'Procedi'){
                    Ext.Ajax.request({
                        url: PretApp.util.Config.getBaseUrl()+'/unusesalelocksale',
                        method: 'GET',
                        disableCaching:false,
                        headers: { 'Content-Type': 'application/json; charset=UTF-8' },
                        params : {
                            userId : window.localStorage.getItem('uid'),
                            saleId : saleData.id
                        },
                        success: function (response) {
                            Ext.Msg.show({
                                message: 'Sconto utilizzato.',
                                buttons:[{
                                    id: 'ok',
                                    iconCls: 'star',
                                    iconMask: true,
                                    text: 'OK',
                                    ui:'confirm'
                                }]
                            });
                            that.getMain().pop(2); // pop to sale list
                        },
                        failure: function (response) {
                            Ext.Msg.show({
                                message: 'Oooops! Si è verificato un errore. Sconto non utilizzato.',
                                buttons:[{
                                    id: 'ok',
                                    iconCls: 'star',
                                    iconMask: true,
                                    text: 'OK',
                                    ui:'confirm'
                                }]
                            });
                            that.getMain().pop(2); // pop to sale list
                        }
                    });
                }
            }
        });
    }
});
