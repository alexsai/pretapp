Ext.define('PretApp.model.Store', {
    extend: 'Ext.data.Model',

    config: {
		idProperty: 'id',
        fields: [ 'id', 'name', 'followers', 'products', 'city', 'address', 'lat', 'lon', 'info', 'logo']
    }

});
