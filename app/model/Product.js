Ext.define('PretApp.model.Product', {
    extend: 'Ext.data.Model',

    config: {
		idProperty: 'id',
        fields: [
            'id',
            'storeId',
            'name',
            'price',
			'imageUrl',
            {name: 'currency', type: 'string', defaultValue: '&euro;'}
        ]
    }

});
