Ext.define('PretApp.model.Sale', {
    extend: 'Ext.data.Model',

    config: {
		idProperty: 'id',
        fields: [ 'id', 'storeId', 'storeName','city', 'amount', 'startDate', 'endDate','expirationDate','conditions', 'unlockedCount',
        	{name:'type', type: 'string', defaultValue:'%'},
        	{name:'state', type: 'string', defaultValue:''}, //   used || unlocked || empty by default
            {name:'userState', type: 'string', defaultValue:''}, // 
        	'daysLeft', 'hoursLeft', 'minutesLeft'
        ]
    },
    setDaysLeft: function() {
        var timeleft = Math.abs( new Date(this.get('endDate').replace(/-/g,'/')) - new Date() );

        var d = Math.floor(timeleft / (24 * 60 * 60 * 1000));
        timeleft = timeleft - (d*(24 * 60 * 60 * 1000));

        var h = Math.floor(timeleft / (60 * 60 * 1000));
        timeleft = timeleft - (h*(60 * 60 * 1000));

        var m = Math.floor(timeleft / (60 * 1000));
        timeleft = timeleft - (m*(60 * 1000));

        this.set('daysLeft', d);
        this.set('hoursLeft', h);
        this.set('minutesLeft', m);
    },
    saleStateTranslate : function(){
        if(this.get('userState')==='unlocked') { this.set('userState','sbloccato'); }
        if(this.get('userState')==='used') { this.set('userState','usato'); }
    },
    saleVendorStateTranslate : function(){
        if(this.get('state')==='SUSPENDED') { this.set('state','sospeso'); }
        if(this.get('state')==='ENABLED') { this.set('state','attivo'); }
    }

});
