Ext.define('PretApp.util.Config', { 
    singleton : true,
    alias : 'widget.appConfigUtil',
        config : {
        // SERVER
        //baseUrl : 'http://pretappenv-nnq2ek6sr3.elasticbeanstalk.com/mobile'
        baseUrl : 'http://localhost:8080/app/mobile',
        baseResourcesUrl : 'http://localhost:8080/app/resources',

        // APP FACEBOOK
        facebookId : '605921656148265',
        facebookPermissions : 'publish_actions',
        facebookRedirect: 'https://www.facebook.com/connect/login_success.html',
        facebookTokenDuration: 110, // minutes
        vendorStoreId: null,
        logged: false,

    /*      
        Additional data defined on facebook login
        window.localStorage.getItem('accessToken');     // string
        window.localStorage.getItem('tokenExpiration'); // Date() 2h
        window.localStorage.getItem('uid');             // string
        window.localStorage.getItem('fbname');          // string
    */

        // Vincoli sconto
        minDaysSaleUnlock: 14,
        minDaysSaleUsage: 14
    },
    constructor: function(config) {
        this.initConfig(config);
        this.callParent([config]);
    },
    isUserLogged: function(){
        console.log(window.localStorage.getItem('accessToken'));
        console.log(window.localStorage.getItem('tokenExpiration'));
        //window.localStorage.setItem('tokenExpiration', new Date(2014,3,30,16,0,0) );
        //console.log(new Date(window.localStorage.getItem('tokenExpiration')) , new Date(), Ext.Date.MINUTE);
        if(window.localStorage.getItem('accessToken') === null ||
            window.localStorage.getItem('accessToken') === undefined ||
            window.localStorage.getItem('tokenExpiration') === null ||
            window.localStorage.getItem('tokenExpiration') === undefined){
            return false;
        }
        if(Ext.Date.diff(new Date(window.localStorage.getItem('tokenExpiration')),new Date(),Ext.Date.MINUTE) > PretApp.util.Config.getFacebookTokenDuration()){
            return false;
        }

        return true;
    },
    /*  
        Source
        http://slavik.meltser.info/phonegap-detect-if-the-application-runs-on-mobile-or-browser-using-javascript-before-the-deviceready-and-document-ready-events-are-triggered/
    */
    isPhoneGap: function() {
        return (window.cordova || window.PhoneGap || window.phonegap)
        && /^file:\/{3}[^\/]/i.test(window.location.href)
        && /ios|iphone|ipod|ipad|android/i.test(navigator.userAgent);
    }
});